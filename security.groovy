#!groovy
 
import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule
import hudson.security.csrf.DefaultCrumbIssuer
import hudson.util.Secret
 
def instance = Jenkins.getInstance()

def env = System.getenv()
def jenkinsPassword = env['JENKINS_PASSWORD']

if (!jenkinsPassword?.trim()) {
  System.err.println("Jenkins admin password is empty. It is expected to be in environment variable JENKINS_PASSWORD")
  System.exit(1)
}
 
def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin", jenkinsPassword)
instance.setSecurityRealm(hudsonRealm)
 
def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()
 
Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)

// CSRF
Jenkins.instance.setCrumbIssuer(new DefaultCrumbIssuer(true))

// Disable old non-encrypted protocols
HashSet<String> newProtocols = new HashSet<>(instance.getAgentProtocols())
newProtocols.removeAll(Arrays.asList(
        "JNLP3-connect", "JNLP2-connect", "JNLP-connect", "CLI-connect"
))
instance.setAgentProtocols(newProtocols)
instance.save()
