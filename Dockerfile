FROM jenkins/jenkins:2.235.1


####### Configure Jenkins
# Do not run the install wizard
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

# Set up users and security
COPY security.groovy /usr/share/jenkins/ref/init.groovy.d/security.groovy

# Disable Jenkins CLI over remoting
COPY jenkins.CLI.xml /usr/share/jenkins/ref/jenkins.CLI.xml

# Define Jenkins url
COPY jenkins.model.JenkinsLocationConfiguration.xml /usr/share/jenkins/ref/jenkins.model.JenkinsLocationConfiguration.xml

# Install plugins
RUN /usr/local/bin/install-plugins.sh git:3.8.0 gradle:1.36 greenballs:1.15 workflow-scm-step:2.11 workflow-aggregator:2.6 job-dsl:1.77

# Git plugin requires global git name and email to be set
COPY hudson.plugins.git.GitSCM.xml /usr/share/jenkins/ref/hudson.plugins.git.GitSCM.xml

# Disable script security for Job DSL scripts (so we can easily generate jobs with job dsl plugin from scm)
COPY javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration.xml /usr/share/jenkins/ref/javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration.xml

# Jenkinsfile pipeline library
COPY org.jenkinsci.plugins.workflow.libs.GlobalLibraries.xml /usr/share/jenkins/ref/org.jenkinsci.plugins.workflow.libs.GlobalLibraries.xml


####### SSH configuration
RUN mkdir /usr/share/jenkins/ref/.ssh

# Create ssh key
RUN ssh-keygen -b 2048 -t rsa -f /usr/share/jenkins/ref/.ssh/id_rsa -q -N ""
RUN chmod 600 /usr/share/jenkins/ref/.ssh/id_rsa

# Add gitlab.com to the known_hosts
USER root
RUN ssh-keyscan -t rsa gitlab.com >> /usr/share/jenkins/ref/.ssh/known_hosts
USER jenkins


####### Create Jenkins jobs
COPY jobs/generate-jobs.config.xml /usr/share/jenkins/ref/jobs/generate-jobs/config.xml

####### Replace any variables on startup (e.g. variables in jobs that need to be set dynamically)
COPY init.groovy /usr/share/jenkins/ref/init.groovy
